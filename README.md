## Uniapp 小程序项目启动模板

一个uniapp的小程序项目启动模板，理论上兼容各家小程序。

不建议用来开发h5或app

<p style="color:#fe7300;font-weight:600">注意：安装依赖前务必确保 @vue/cli 版本是 4.x</p>

```
// 检查@vue/cli版本
vue -V

// 卸载@vue/cli
npm remove -g @vue/cli

// 安装@vue/cli 4.x
npm i -g @vue/cli@4
```

#### 特性

- uniapp-cli 模式，不依赖Hbuilder
- 集成 tailwind.css
- UI库：uview、vant
- 统一入口页实现全局登陆拦截
- 依赖体积分析

#### 配置启动步骤

1. 配置小程序appid，位于 /src/manifest.json 中的 mp-weixin.appid。(同时建议配置项目名称 name)
2. 配置API地址，位于 /src/config.js
3. 配置iconfont图标，在 iconfont.cn 中复制图片集，粘贴到 /src/css/iconfont.css，并批量修改 //at.alicdn.com 为 https://at.alicdn.com。使用图标参考 /src/components/t-components/t-icon
4. 使用图片，图片肯定不能放在项目中（体积过大），引用图片可调用 $this.useImage() 方法，定义于 /src/libs/image-map.js 

#### npm指令

```
# 安装依赖
npm i

# 启动开发
npm run dev:mp-weixin

# 启动打包
npm run build:mp-weixin

# 真机调试或预览
npm run preview:mp-weixin

# 启动依赖分析，注意不要占用8888端口，或在vue.config.js中可以自己修改端口
npm run analyzer:mp-weixin

```

#### 注意事项

##### tailwind.css过大导致无法预览和真机调试

由于tailwind.css只在打包的时候才做摇树优化，开发模式下全包约3M，会导致npm run dev开发模式下体积超限无法进行真机调试或预览。这时可以用 npm run preview:mp-weixin 指令来预览和调试，原理就是使用生产环境模式（NODE_ENV=production），同时启动文件修改监听（-- watch）。

npm run preview:mp-weixin 模式编译较慢，建议仅在需要预览和真机调试时使用。

##### 依赖分析

采用 webpack-bundle-analyzer 进行依赖体积分析，在vue.config.js中使用，当读取到环境变量 ANALYZER=1 时启动。

可以直接使用 npm run analyzer:mp-weixin 指令来打开
```
