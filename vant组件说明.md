### 关于vant-weapp组件

放在wxcomponents目录中的一切文件，无论是否使用，都会被打包。
整个vant所有组件体积为374k，可使用小程序的依赖分析查看

	考虑到体积优化问题，本仓库将完整的vant组件放在外层的 vant 目录中。
	如需使用再自行移动到 wxcomponents/vant/ 目录中


### 如何使用

例如使用 \<van-button> 组件

```
1. 从 /vant 中复制 button 目录到 /src/wxcomponents/vant/ 中

2. 在 /src/page.json 中配置
"globalStyle": {
    "usingComponents": {
        "van-button": "/wxcomponents/vant/button/index",
    }
}

3. 检查该组件的依赖项
有些组件依赖了common/中的js，有些组件则依赖了其他组件
可通过该组件中的 xxx.json 查看，或直接看编译器提示
这里button组件这里依赖了loading和icon组件，也需要一并移入并在page.json中配置
一般来说，这几个目录是所有组件都依赖的
common、wxs、mixins


4. 页面中使用 <van-button>按钮</van-button>
```