const { createPreset } = require('tailwindcss-miniprogram-preset')

/**
 * 使用 tailwindcss-miniprogram-preset 能基本满足rem转rpx的问题
 * 其转换比例是 默认 rootValue=32 相当于 m-1 等于 margin:8rpx
 * 这个一般不建议修改，因为间距除了margin padding还有font-size和line-height等，一旦改了都会变
 * 如果项目有自己的需求还是建议自己配置一套间距预设
 */

module.exports = {
	presets: [
		createPreset({ rem2rpx: true, rootValue: 32 }), //rem转rpx
	],
	purge: {
		// 如果开发模式过大，可以一直开启enable
		enabled: process.env.NODE_ENV === 'production',
		content: ['./public/index.html', './src/**/*.{vue,js,ts,jsx,tsx,wxml}'],
	},
	darkMode: false, // or 'media' or 'class'
	separator: '__', // 兼容小程序，将 : 替换成 __
	theme: {
		extend: {},
		screens: false,
	},
	variants: {
		extend: {},
	},
	plugins: [],
	corePlugins: {//禁用一些小程序class不支持的分割
		space: false,
		divideWidth: false,
		divideColor: false,
		divideStyle: false,
		divideOpacity: false,
		preflight: false,
	}
}
