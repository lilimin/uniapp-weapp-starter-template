## 注意事项

node 版本经测试可以是12.18.3，也可以是16.14.0
注意确保 @vue/cli 的版本是4.x，使用前可以打开当前node目录，查看全局node_modules中的 @vue/cli 版本
确保项目是由 @vue/cli 4.x版本创建的，否则首次可能可以启动，之后安装其他依赖就会出问题。如果出现类似 webpack/libs/RuleSet 不存在之类报错，就是这个问题。应该重新创建项目进行迁移

## uniapp cli模式如何升级编译器

参考[uniapp-cli文档](https://uniapp.dcloud.net.cn/quickstart-cli.html)
可以使用 @dcloudio/uvm 管理编译器的版本

