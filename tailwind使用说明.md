### 在uniapp/小程序中使用tailwindcss

## 安装tailwindcss的步骤

	注意：uniapp-cli内置了sass 7.x版本（在），所以不要自己装8.x
	相应的tailwindcss也需要装对应postcss 7.x版本的兼容版
	参考：https://blog.csdn.net/qq_43869822/article/details/122372587


1. 安装uniapp框架，[参考文档](https://uniapp.dcloud.net.cn/quickstart-cli.html#创建uni-app)

2. 安装tailwindcss，[参考文档](https://blog.csdn.net/qq_43869822/article/details/122372587)

```
npm install tailwindcss@npm:@tailwindcss/postcss7-compat -D
```
	
3. 根目录创建tailwind.config.js

```
// tailwind.config.js
const { createPreset } = require('tailwindcss-miniprogram-preset')
module.exports = {
  purge: ['./public/index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false,
  separator: '__', // 兼容小程序，将 : 替换成 __
  presets: [
    createPreset({ rem2rpx: true, rootValue: 16 }), //rem转rpx
  ],
  theme: {
    // 兼容小程序，将默认配置里带 .和/ 清除
    // 如果有些属性你没有用到，请在 corePlugins 里禁用
    extend: {},
    fontSize: {},
    height: {},
    inset: {},
    screens: {},
    spacing: {},
    translate: {},
    width: {}
  },
  variants: {},
  plugins: [],
  corePlugins: {
    // 兼容小程序，将带有 * 选择器的插件禁用
    preflight: false,
    space: false,
    divideColor: false,
    divideOpacity: false,
    divideStyle: false,
    divideWidth: false
  }
}
```

4. 配置postcss

```
// postcss.config.js
const path = require('path')
module.exports = {
  parser: require('postcss-comment'),
  plugins: [
    require('postcss-import')({
      resolve(id) {
        if (id.startsWith('~@/')) {
          return path.resolve(process.env.UNI_INPUT_DIR, id.substr(3))
        } else if (id.startsWith('@/')) {
          return path.resolve(process.env.UNI_INPUT_DIR, id.substr(2))
        } else if (id.startsWith('/') && !id.startsWith('//')) {
          return path.resolve(process.env.UNI_INPUT_DIR, id.substr(1))
        }
        return id
      }
    }),
    // 主要是添加这两项
    require('tailwindcss'),
    ...(
        process.env.UNI_PLATFORM !== "h5"
        ? [
          // 使用postcss-class-name 包将小程序不支持的类名写法转换为支持的类名，如："hover:xxx"
          require("postcss-class-rename")({
	        '\\\\.': '_' // 兼容小程序，将类名带 .和/ 替换成 _
          })
        ]
       : [
          require("autoprefixer")({
            remove: process.env.UNI_PLATFORM !== 'h5',
          }),
       ]
    ),
    // 注意这一项是uniapp自带的，必须写在最后
    require('@dcloudio/vue-cli-plugin-uni/packages/postcss')
  ]
}
```

5. 创建tailwind.css并引入

```
// 创建 /src/tailwind.css 文件，写入如下

/* 这两条小程序中不需要 */
/* @tailwind base; */
/* @tailwind components; */

@tailwind utilities;

```

```
// 建议在App.vue中的style部分引入此文件
// /src/App.vue
<style>
@import url("./tailwind.css");
</style>
```

至此页面中应该可以正常使用tailwind.css了，在开发模式可以看到编译出了完整的tailwind.css，打包后会根据tailwind.config.js的配置做摇树优化