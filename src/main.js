import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

App.mpType = 'app'

// 实例化App
import store from "./store";
const app = new Vue({
	...App,
	store
})

// 安装iview
import uView from "uview-ui";
Vue.use(uView);

// 挂载全局导航方法
import { navigateTo, navigateBack } from "@/libs/navigate";
import useImage from '@/libs/image-map'
Vue.prototype.$navigateTo = navigateTo;
Vue.prototype.$navigateBack = navigateBack;
Vue.prototype.$useImage = useImage;

// 挂载filters
import filters from "@/libs/filters";
Vue.use(filters)

// 设置dayjs语言
import 'dayjs/locale/zh-cn'
import * as dayjs from 'dayjs'
import duration from 'dayjs/plugin/duration'
import RelativeTime from 'dayjs/plugin/relativeTime'
import isoWeek from 'dayjs/plugin/isoWeek'
dayjs.locale('zh-cn')
dayjs.extend(duration);
dayjs.extend(RelativeTime);
dayjs.extend(isoWeek);

app.$mount()
