import beforeRequest from "./beforeRequest.js";
import afterResponse from "./afterResponse.js";

export default async (config) => {
	config = await beforeRequest(config);
	return uni.request(config).then(([error, response]) => {
		return afterResponse(config, error, response);
	});
};

export const upload = async (config) => {
	config.is_upload = true;
	config = await beforeRequest(config);
	return uni.uploadFile(config).then(([error, response]) => {
		try {
			response.data = JSON.parse(response.data);
		} catch (e) { }
		return afterResponse(config, error, response);
	});
};
