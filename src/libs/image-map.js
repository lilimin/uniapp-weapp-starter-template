/**
 * 所有图片资源的地址路径
 */

const imageMap = [
	//#region 首页部分
	{
		key: 'home-',
		name: '首页顶部banner',
		url: 'https://20tangzhan.oss-cn-shenzhen.aliyuncs.com/image/0b/b4f21f497538854d283c6f16428c50b5668e79.png'
	},
	//#endregion

	//#region 
	{
		key: 'user-home-banner',
		url: 'https://gwxt-oss-main.oss-cn-shanghai.aliyuncs.com/image/97/cc46b2da8b8eb926c0396d5041dbd48c0561507619fff621dec0b3c0280e13.jpeg'
	}
	//#endregion
]

/**
 * 调用图片的方法
 * @param {string} key 根据图片key查询
 * @param {string} name 根据图片name查询，此时需要key是undefined
 * @param {boolean} onlyUrl [true] 是否仅返回url
 * @returns {string|object}
 */
const useImage = (key, name = '', onlyUrl = true) => {
	let find = typeof key == 'undefined'
		? imageMap.find(i => i.name == name)
		: imageMap.find(i => i.key == key)

	if (!find) return null;
	return onlyUrl ? find.url : find;
};

export default useImage