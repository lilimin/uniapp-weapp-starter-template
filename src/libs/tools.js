/**
 * 节流函数
 * @description 本函数接收一个Promise，当Promise在pending状态中，会自动阻止下次继续触发，在finally后释放。同时允许传入一个时限，如果达到时限Promise还未finally，会执行onTime回调
 * @param {string|number} promise_id 节流ID，同ID的内部节流，不同ID的互不干扰
 * @param {Function} promiseFunction 要节流的Promise函数，该函数应该返回一个Promise
 * @param {number} time x毫秒后触发onTime，如果在此之前Promise已经finally，则不执行onTime
 * @param {Function} onTime 事件
 * @returns 
 */
export const promiseThrottle = (promise_id, promiseFunction, time = 500, onTime = null) => {
	if (_promiseThrottleMap[promise_id] === true) return Promise.reject(Error('请求正在进行中'));

	let timeout = undefined;
	if (typeof time == 'number') timeout = setTimeout(onTime, time);

	_promiseThrottleMap[promise_id] = true;
	return promiseFunction().finally(() => {
		_promiseThrottleMap[promise_id] = false;
		clearTimeout(timeout)
	});
}
let _promiseThrottleMap = {};


/**
 * 转换day_good表的数据为good数据
 * @param {object} day_good 数据库的day_good模型
 */
export const transDayGoodToGood = (day_good) => {
	return ({
		...day_good,
		good: {
			id: day_good.g_id,
			name: day_good.g_name,
			desc: day_good.g_desc,
			cover_image_url: day_good.g_cover_image_url,
			category_type_id: day_good.g_category_type_id,
			time_type_ids: day_good.g_time_type_ids,
			detail_images: day_good.g_detail_images,
			price: day_good.g_price,
			sort: day_good.g_sort,
		}
	})
}