
// API基础路径
export const API_BASE_URL = process.env.NODE_ENV == 'production'
	? '' 	// 生产环境API地址
	: '';	// 开发环境API地址

export default { API_BASE_URL };
