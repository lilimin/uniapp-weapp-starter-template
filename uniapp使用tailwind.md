# 简介

使用Hbuilder创建的uniapp工程，目前很难找到可以顺利使用[tailwindcss](https://tailwindcss.com/)的方案

本文仅针对 Hbuilder 创建的 uniapp 工程，对于 [vue-cli](https://uniapp.dcloud.net.cn/quickstart-cli.html) 方式创建的uniapp工程，网上有文章提供了方法，但**也可以使用本文的方法实现**

本文的方法目前看来是最优雅的契合uniapp的，当然这得益于tailwindcss提供了强大的解决方案 —— [Tailwind CLI](https://tailwindcss.com/docs/installation)

# 先说如何使用

总共5步

1. 安装tailwindcss

```bash
npm install tailwindcss -D
```

注意最新的tailwindcss已经升级到 3.x 版本，这里安装的就是 3.x ，文档：https://tailwindcss.com/
而中文版的tailwindcss文档目前是 2.x 版本，有所差异，建议使用最新的

2. 创建输入css文件

在根目录创建 tailwind-input.css，并写上如下内容

```css
@tailwind base;  /* 如果是小程序的话，这一行注释掉，因为tailwind base模块提供的一些样式选择器是基于*，这在小程序中会报错 */
@tailwind components;
@tailwind utilities;
```

3. 创建tailwind配置文件

在根目录创建 tailwind.config.js 配置文件，可以使用`npx tailwind init`指令，也可以自己创建，写上如下内容

```javascript
/** @type {import('tailwindcss').Config} */
module.exports = {
    separator: '__', // 如果是小程序项目需要设置这一项，将 : 选择器替换成 __，之后 hover:bg-red-500 将改为 hover__bg-red-500
    corePlugins: {
        // 预设样式
        preflight: false, // 一般uniapp都有预设样式，所以不需要tailwindcss的预设

        // 以下功能小程序不支持
        space: false, // > 子节点选择器
        divideWidth: false,
	divideColor: false,
	divideStyle: false,
	divideOpacity: false,
    },

    // 指定要处理的文件
    content: [
        './pages/**/*.{vue,js}',
        './main.js',
        './App.vue',
        './index.html'
    ],
    theme: {
        // 字号，使用 App.vue 中的 --x-font-size 样式变量配置
        fontSize(config){
            const list = ['2xs','xs','sm','base','md','lg','xl','2xl','3xl'];
            let result = {}
            list.forEach(it=>{
                result[it] = `var(--x-font-size-${it})`
            })
            return result
        },
        extend: {
            // 间距，tailwindcss中默认间距是rem单位，可以统一设置为uniapp的rpx单位。
           // 类似的设置根据项目需求自己调整一下就好了，没必要去安装别人的预设，其实主要是小程序不兼容的css比较多，H5和App基本都直接兼容tailwindcss默认的预设
            spacing(config) {
                let result = { 0: '0' }
                // 允许的数值大一些也无所谓，最后打包tailwindcss会摇树优化，未使用的样式并不会打包
                for (let i = 1; i <= 300; i++) {
                    result[i] = `${i}rpx`
                }
                return result
            },
            // 增加颜色板，现在主流UI组件库大都是采用css变量实现定制主题，所以这里引用了全局的css变量，这个css变量的定义位置可以在 App.vue 中 page{} 选择器下
            // 其实tailwindcss只是一个css工具，不必局限于它内部提供的东西，灵活运用css变量这些特性完全可以整合出自己的生产力工具
            colors:{ 
                'primary': 'var(--x-color-primary)',
                'tips' : 'var(--x-color-tips)'
            },
        },
    },
    plugins: [],
}
```

4. 启动 tailwind cli 输出结果css

```bash
# 在根目录
npx tailwindcss -i ./tailwind-input.css -o ./static/tailwind.css --watch
```

这行指令意思是，启动 tailwind cli，以 ./tailwind-input.css 为输入文件，以 ./tailwind.config.js （默认）为配置文件，开始扫描配置中 content 字段指定的【项目文件】，并输出结果css文件到 ./static/tailwind.css 中，并且监听这些【项目文件】的变化，实时更新输出结果文件

5. 在项目中引入 tailwind cli 的输出结果

在 main.js 或 App.vue 中引入都行，例如在main.js中

```javascript
import "@/static/tailwind.css"
```

**大功告成！**

# 总结一下

实际上tailwindcss有几种使用方式，传统的用法是利用 postcss ，把tailwindcss当作postcss的一个插件使用。

而uniapp的情况比较特殊，实际上使用HbuilderX创建的uniapp工程也是内置了postcss的（似乎是7.x版本），这点uniapp文档提及较少，只找到一处（https://uniapp.dcloud.net.cn/tutorial/adapt.html#_3-内容缩放拉伸的处理 ）说明。

实际上因为HbuilderX创建的uniapp内部做了高度封装，所以想自己接入它的postcss过程，反而路走偏了，光是postcss v7.x 和 v8.x 的差异，都足以大到必须安装不同版本的tailwindcss才能解决

所以直接使用tailwindcss的cli方式，它自身可以在不依赖postcss和项目的情况下，自己处理好css转换的事。

归根结底，其实 tailwind cli 的工作很简单

1. 从一个入口css（tailwind-input.css）开始（读取其中的 @tailwind utilities; 指令）
2. 经过配置文件（tailwind.config.js），知道要处理哪些目标【项目文件】，以及要启用哪些功能
3. 看看【项目文件】中使用了哪些功能类（其实它的摇树优化就是单纯的字符串匹配）
2. 输出一个结果css（static/tailwind.css）

它要做的事，本身就是这么简单

# 高效自动化

经过上述操作，项目还是有一点点麻烦，即每次用Hbuilder启动工程开发之前，需要手动执行 `npx tailwindcss -i xxxx`。

既然是程序，它完全应该更自动化一点。只需两步

1. `npx tailwindcss -i xxxx` 这个命令可以放进package.json中作为scripts执行

```json
// 根目录的 package.json 文件
// 什么？你的项目没有这个文件？  ... 那在根目录执行 npm init 命令就有了
{
  // ...
  "scripts": {
    "tailwind-dev": "tailwindcss -i ./tailwind-input.css -o ./static/tailwind.css --watch",
    "tailwind-build": "tailwindcss -i ./tailwind-input.css -o ./static/tailwind.css",
  },
  // ...
}

```

这样一来下次项目启动开发前，可以直接 `npm run tailwind-dev` 启动开发，`npm run tailwind-build` 启动生产环境打包

2. 在Hbuilder启动项目开发和打包项目的过程中，让它自己执行这个npm命令

这一步稍微有点复杂，因为Hbuilder在编辑器内部集成了vue-cli或vite环境，并不能直接去控制它的启动开发过程或打包过程。

但有一点啊，uniapp一定是基于vue的，那么vue一定需要vue-cli（vue2）或vite（vue3）来打包。vue-cli 和 vite，都有配置文件，可以自由配置（参考：https://uniapp.dcloud.net.cn/collocation/vite-config.html# ）。

这里以vue3的 `vite.config.js` 举例，vue2的 `vue.config.js` 是同理的

```javascript
// vite.config.js
import { defineConfig } from 'vite';
import uni from '@dcloudio/vite-plugin-uni';

/** ==== 处理 tailwind cli 的自动启动和打包 ==== */
const child_process = require('child_process')
let tailwindMode = process.env.NODE_ENV

// 主进程输出
console.log(`[tailwindcss] 开始${tailwindMode == 'production' ? '生产环境打包' : '开发模式监听'}`);
child_process.exec(
	// 这里指令对应 package.json 中的 npm scripts
	tailwindMode == 'production'
		? 'npm run tailwind-build'
		: 'npm run tailwind-dev',
	{
		cwd: __dirname, // 切换目录到当前项目，必须
	},
	(error, stdout, stderr) => {
		// tailwind --watch 是一个持久进程，不会立即执行回调
		// process.stdout.write('tailwind success')
		if (error) {
			console.error('[tailwindcss] 异常，请检查');
			console.error(error);
			console.error(stdout);
			console.error(stderr);
		}
		if(tailwindMode == 'production'){
			console.log('[tailwindcss] 生产环境打包完成');
		}
	})

export default defineConfig({
	plugins: [uni()],
});
```

对，大概就是这样。现在试一下用Hbuilder启动开发，然后在页面中使用tailwindcss的样式，你会发现 static/tailwind.css 这个文件被实时更新了。


# 写在后面的话

能让程序干的活就应该让程序干

我开源了一个uniapp的小程序工程启动模板
（ https://gitee.com/lilimin/uniapp-weapp-starter-template ）

实现了 

- tailwindcss 整合
- vant-weapp 整合（个人觉得vant组件都同时支持受控和非受控模式，相比一些uniapp生态的UI组件库，这个设计模式用着还是比较舒服的）

，这个项目主要是自己用，未必会长期维护，有兴趣可以点个赞~

（本文原创，于2022年10月17日 发布到 DCloud 论坛 https://ask.dcloud.net.cn/article/40098）